colorama==0.4.4
Jinja2==3.0.1
loguru==0.5.3
MarkupSafe==2.0.1
win32-setctime==1.0.3
