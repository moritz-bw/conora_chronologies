# Introduction

This project generates a static html using jinja2

# Getting Started

## Prerequisites

You need

- Python>=3.6. I used 3.8
- [pip](https://pypi.org/project/pip/)
- Some backend which generates html like data. I used [my Covid country comparison](https://github.com/region-spotteR/conora_chronologies)

## Installing

Load the dependent packages using

```
pip install requirements.txt
```

In addition you need to install all the python packages which your backend uses e.g. `pip install -r backend/requirements.txt`.

# Usage/Contribute

Adapt generator.py to your needs. A minimal example which I used to start is [this repo](https://gist.github.com/mattvh/921dbd9ee2698a7dd8190a67c471c0aa)
